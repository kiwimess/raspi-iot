from picamera import PiCamera
from time import sleep
import RPi.GPIO as GPIO
import time

LED=5
PIR=18

GPIO.setmode (GPIO.BCM)
GPIO.setup (LED, GPIO.OUT)
GPIO.setup (PIR, GPIO.IN)

camera = PiCamera()

finish = time.time() +1000
while time.time() < finish:
    if GPIO.input (PIR):
	print ("Liiketta!")
    	GPIO.output (LED, 1)

	camera.start_preview()
	sleep(2)
	timestr = time.strftime("%Y%m%d-%H%M%S")
	camera.capture('/home/pi/raspi-iot/camera/photos/image'+timestr+'.jpg')
	camera.stop_preview()
	sleep(2)
	GPIO.output (LED, 0)
    else:
	print ("ei liiketta")
	sleep(2)
GPIO.cleanup ()
