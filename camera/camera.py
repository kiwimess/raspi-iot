from picamera import PiCamera
from time import sleep
import time

camera = PiCamera()
camera.start_preview()
sleep(2)
timestr = time.strftime("%Y%m%d-%H%M%S")
camera.capture('/home/pi/raspi-iot/camera/photos/image'+timestr+'.jpg')
camera.stop_preview()
