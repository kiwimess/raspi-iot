import RPi.GPIO as GPIO
import time

LED=4
PAINIKE=16

GPIO.setmode (GPIO.BCM)
GPIO.setup (LED, GPIO.OUT)
GPIO.setup (PAINIKE, GPIO.IN)

loppu = time.time() + 10
while time.time() < loppu:
	GPIO.output(LED, 1)
	time.sleep (0.5) # ilman tata prossukaytto 100%
	GPIO.output(LED, 0)
	time.sleep (0.5)
GPIO.cleanup ()
