import RPi.GPIO as GPIO
import time

JALANVIH=5
JALANPUN=6
AUTOPUN=13
KELTAINEN=19
AUTOVIH=26

PAINIKE=16
LIIKE=4

GPIO.setmode (GPIO.BCM)
GPIO.setup (JALANPUN, GPIO.OUT)
GPIO.setup (JALANVIH, GPIO.OUT)
GPIO.setup (AUTOPUN, GPIO.OUT)
GPIO.setup (KELTAINEN, GPIO.OUT)
GPIO.setup (AUTOVIH, GPIO.OUT)
GPIO.setup (PAINIKE, GPIO.IN)
GPIO.setup (LIIKE, GPIO.IN)

GPIO.output(JALANVIH, 0)
GPIO.output(JALANPUN, 1)
GPIO.output(AUTOPUN, 0)
GPIO.output(AUTOVIH, 1)
GPIO.output(KELTAINEN, 0)

autoliikennetta = False
autollevihrea = True
jalallevihrea = False

def autovihrea():
    GPIO.output(JALANVIH, 0)
    GPIO.output(JALANPUN, 1)
    time.sleep (0.5)
    GPIO.output(AUTOPUN, 0)
    GPIO.output(AUTOVIH, 1)
    autollevihrea = True
    jalallevihrea = False
    time.sleep(2)

def jalanvihrea():
    GPIO.output(AUTOVIH, 0)
    GPIO.output(AUTOPUN, 1)
    time.sleep (0.5)
    GPIO.output(JALANPUN, 0)
    GPIO.output(JALANVIH, 1)
    GPIO.output (KELTAINEN, 0)
    print ("Jalankulkijan vihrea 5s")
    time.sleep(5)
    jalallevihrea = True
    autollevirhea = False

loppu = time.time() + 60
while time.time() < loppu:
    print ("luuppi alkaa")
    if GPIO.input (LIIKE):
	autoliikennetta = True
	print ("autoliikennetta")
	time.sleep(1)
    else:
	autoliikennetta = False

    if GPIO.input (PAINIKE):
	print ("jalankulkijan painike")
	GPIO.output (KELTAINEN, 1)
	if autoliikennetta:
	    print ("autoliikennetta, odotetaan 5s")
	    time.sleep(5)
	else:
	    print ("ei autoja")
	jalanvihrea()
    else:
	print ("autoille vihrea")
	autovihrea()
    time.sleep (0.1) # ilman tata prossukaytto 100%
GPIO.cleanup ()
