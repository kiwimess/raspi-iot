import RPi.GPIO as GPIO
import time

JALANVIH=5
JALANPUN=6
AUTOPUN=13
KELTAINEN=19
AUTOVIH=26

PAINIKE=16

GPIO.setmode (GPIO.BCM)
GPIO.setup (JALANPUN, GPIO.OUT)
GPIO.setup (JALANVIH, GPIO.OUT)
GPIO.setup (AUTOPUN, GPIO.OUT)
GPIO.setup (KELTAINEN, GPIO.OUT)
GPIO.setup (AUTOVIH, GPIO.OUT)
GPIO.setup (PAINIKE, GPIO.IN)

GPIO.output(JALANVIH, 0)
GPIO.output(JALANPUN, 1)
GPIO.output(AUTOPUN, 0)
GPIO.output(AUTOVIH, 1)
GPIO.output(KELTAINEN, 0)

def autovihrea():
	GPIO.output(JALANVIH, 0)
	GPIO.output(JALANPUN, 1)
	time.sleep (1)
	GPIO.output(AUTOPUN, 0)
	GPIO.output(AUTOVIH, 1)

def jalanvihrea():
	GPIO.output(AUTOVIH, 0)
	GPIO.output(AUTOPUN, 1)
	time.sleep (1)
	GPIO.output(JALANPUN, 0)
	GPIO.output(JALANVIH, 1)

loppu = time.time() + 20
while time.time() < loppu:
	if GPIO.input (PAINIKE):
		jalanvihrea()
	else:
		autovihrea()
	time.sleep (0.1) # ilman tata prossukaytto 100%
GPIO.cleanup ()

