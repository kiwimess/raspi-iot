# Tuodaan kirjasto koodiin.
import RPi.GPIO as GPIO
# Asetetaan BCM-numerointi
GPIO.setmode(GPIO.BCM)
# Asetetaan pinni 4 ulostulotilaan 
GPIO.setup(4, GPIO.OUT)
GPIO.setup(6, GPIO.IN)
# Laitetaan pinniin 4 virtaa
GPIO.output(4, 1) 
GPIO.input(6)
GPIO.cleanup()
